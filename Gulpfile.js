
var gulp = require('gulp');
var sass = require('gulp-sass');


compile = function() {
    return new Promise(function(resolve, reject) {
        gulp.src('./*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'));
        resolve();
    });
}

gulp.task('default', compile);

